output "metric_filter_map" {
  value = { for k, v in google_logging_metric.mandatory_log_metrics : k => v.filter }
}
output "project_number" {
  value = data.google_project.project.number
}

output "project_name" {
  value = data.google_project.project.name
}
