# Changelog

All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).
This changelog is generated automatically based on [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).

## [Unreleased]

## [0.1.1](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_metric_alerts/-/tree/v0.1.0) - 2021-08-24

### Changed
- updated developer tools to version 1

## [0.1.0](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_metric_alerts/-/tree/v0.1.0) - 2021-12-28

### Added
- Adds the mandatory_alert_policies


## [0.0.0](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_metric_alerts/-/tree/v0.0.0) - 2021-03-21

### Features
- Initial release
