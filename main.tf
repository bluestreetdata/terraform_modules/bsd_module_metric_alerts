locals {

  metrics_alerts = {
    "project-ownership-assignments-changes" = {
      display_names      = "Project Ownership Assignments/Changes",
      metric_filters     = "(protoPayload.serviceName=\"cloudresourcemanager.googleapis.com\") AND (ProjectOwnership OR projectOwnerInvitee) OR (protoPayload.serviceData.policyDelta.bindingDeltas.action=\"REMOVE\" AND protoPayload.serviceData.policyDelta.bindingDeltas.role=\"roles/owner\") OR (protoPayload.serviceData.policyDelta.bindingDeltas.action=\"ADD\" AND protoPayload.serviceData.policyDelta.bindingDeltas.role=\"roles/owner\")",
      metric_description = "Project owner assignment/change actions monitoring.",
    },
    "audit-configuration-changes" = {
      display_names      = "Audit Configuration Changes",
      metric_filters     = "protoPayload.methodName=\"SetIamPolicy\" AND protoPayload.serviceData.policyDelta.auditConfigDeltas:*",
      metric_description = "Cloud audit logging provides a history of GCP API calls for an account.",
    },
    "custom-role-changes" = {
      display_names      = "Custom Role Changes",
      metric_filters     = "resource.type=\"iam_role\" AND protoPayload.methodName=\"google.iam.admin.v1.CreateRole\" OR protoPayload.methodName=\"google.iam.admin.v1.DeleteRole\" OR protoPayload.methodName=\"google.iam.admin.v1.UpdateRole\"",
      metric_description = "Counts changes to IAM role creation, deletion and updating activities.",
    },
    "vpc-network-firewall-rule-changes" = {
      display_names      = "VPC Network Firewall Rule Changes",
      metric_filters     = "resource.type=\"gce_firewall_rule\" AND jsonPayload.event_subtype=\"compute.firewalls.patch\" OR jsonPayload.event_subtype=\"compute.firewalls.insert\"",
      metric_description = "Monitoring Virtual Private Cloud (VPC) Network Firewall rule changes.",
    },
    "vpc-network-route-changes" = {
      display_names      = "VPC Network Route Changes",
      metric_filters     = "resource.type=\"gce_route\" AND jsonPayload.event_subtype=\"compute.routes.delete\" OR jsonPayload.event_subtype=\"compute.routes.insert\"",
      metric_description = "Monitoring Virtual Private Cloud (VPC) router changes.",
    },
    "vpc-network-changes" = {
      display_names      = "VPC Network Changes",
      metric_filters     = "resource.type=gce_network AND jsonPayload.event_subtype=\"compute.networks.insert\" OR jsonPayload.event_subtype=\"compute.networks.patch\" OR jsonPayload.event_subtype=\"compute.networks.delete\" OR jsonPayload.event_subtype=\"compute.networks.removePeering\" OR jsonPayload.event_subtype=\"compute.networks.addPeering\"",
      metric_description = "Monitoring Virtual Private Cloud (VPC) network changes.",
    },
    "cloud-storage-iam-permission-changes" = {
      display_names      = "Cloud Storage IAM Permission Changes",
      metric_filters     = "resource.type=gcs_bucket AND protoPayload.methodName=\"storage.setIamPermissions\"",
      metric_description = "Monitoring Cloud Storage IAM permission changes",
    },
    "cloud-sql-instance-configuration-changes" = {
      display_names      = "Cloud SQL Instance Configuration Changes",
      metric_filters     = "protoPayload.methodName=\"cloudsql.instances.update\"",
      metric_description = "Monitoring changes to Cloud SQL instance configuration changes.",
    },
  }
}

resource "google_logging_metric" "mandatory_log_metrics" {
  for_each    = local.metrics_alerts
  name        = each.key
  filter      = each.value.metric_filters
  project     = var.project_id
  description = each.value.metric_description
  metric_descriptor {
    metric_kind  = "DELTA"
    value_type   = "INT64"
    display_name = each.value.display_names
  }
}

data "google_project" "project" {
  project_id = var.project_id
}

# https://github.com/hashicorp/terraform-provider-google/issues/2605
resource "google_monitoring_alert_policy" "mandatory_alert_policies" {
  depends_on   = [google_logging_metric.mandatory_log_metrics]
  for_each     = local.metrics_alerts
  display_name = each.value.display_names
  combiner     = "OR"
  project      = var.project_id
  enabled      = true
  conditions {
    display_name = each.value.display_names
    condition_threshold {
      filter          = "metric.type=\"logging.googleapis.com/user/${each.key}\" resource.type=\"audited_resource\""
      threshold_value = 0.001
      duration        = "0s"
      comparison      = "COMPARISON_GT"
      aggregations {
        alignment_period     = "60s"
        cross_series_reducer = "REDUCE_COUNT"
        per_series_aligner   = "ALIGN_RATE"
      }
    }
  }
}
