


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| project\_id | The project in which the metric alerts will be created | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| metric\_filter\_map | n/a |
| project\_id | The ID of the project in which resources are provisioned. |
| project\_number | n/a |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->