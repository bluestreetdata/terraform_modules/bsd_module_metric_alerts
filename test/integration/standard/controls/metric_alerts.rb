title 'Testing mertric alerts have been set up correctly'

PROJECT_NAME = attribute('project_name', description: 'gcp project name')
control 'metric-alerts' do
  impact 0.7
  title 'Check development project'
  describe google_project(project: attribute("project_id")) do
    it { should exist }
    its('name') { should eq PROJECT_NAME }
  end
end